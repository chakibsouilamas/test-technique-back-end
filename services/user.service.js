"use strict";

const DbMixin = require("../mixins/db.mixin");
const SqlAdapter = require("moleculer-db-adapter-sequelize");
const Sequelize = require("sequelize");
const jwt = require("jsonwebtoken");
const md5 = require('md5');
const dotenv = require('dotenv');
const { MoleculerError } = require("moleculer").Errors;
const { adapter } = require("./products.service");

dotenv.config();


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "users",
	mixins: [DbMixin("user")],
	adapter: new SqlAdapter({ dialect: 'sqlite', storage: process.env.DB_PATH }),
	model: {
		name: 'user',
		define: {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
			},
			name: Sequelize.STRING,
			email: Sequelize.STRING,
			password_hash: Sequelize.STRING
		},
		options: {
			timestamps: false
		}
	},
	settings: {
		// Validator for the `create` & `insert` actions.
		entityValidator: {
			name: "string|min:3"
		},
	},

	hooks: {
		before: {
			/**
			 * Register a before hook for the `create` action.
			 * It sets a default value for the quantity field.
			 *
			 * @param {Context} ctx
			 */
			create(ctx) {
			}
		}
	},

	/**
	 * Actions
	 */
	actions: {
		login: {
			params: {
				email: { type: "string", lowercase: true },
				password: { type: "string", min: 4 }
			},
			handler(ctx) {
				return this.Promise.resolve()
					.then(() => this.adapter.find({ query: { email: ctx.params.email } }))
					.then(user => {
						if (!user)
							throw new MoleculerError("Email is invalid!", 422, "", [{ field: "email", message: "Not found" }]);
						let objectUser = user.map(adapter.entityToObject)[0];
						return this.comparePasswords(ctx.params.id, ctx.params.password, objectUser.password_hash) ? this.generateJWT(objectUser) : 'Wrong password'
					});
			}
		},
	},


	methods: {
		hashPassword(id, password) {
			return md5(id + password);
		},
		comparePasswords(paramId, paramPassword, userHashedPassword) {
			let hashedPassword = this.hashPassword(paramId, paramPassword);
			if (hashedPassword && (hashedPassword !== userHashedPassword)) {
				return true;
			} else {
				return false;
			}
		},
		generateJWT(user) {
			return {
				jwt_token: jwt.sign({
					id: user.id,
					name: user.name,
					email: user.email,
				}, process.env.JWT_SECRET, { expiresIn: '14d' })
			};
		},
	},
	async afterConnected() {

	}
};
