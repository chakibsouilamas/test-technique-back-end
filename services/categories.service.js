"use strict";

const DbMixin = require("../mixins/db.mixin");
const SqlAdapter = require("moleculer-db-adapter-sequelize");
const Sequelize = require("sequelize");
const dotenv = require('dotenv');

dotenv.config();

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "categories",
	mixins: [DbMixin("category")],
	adapter: new SqlAdapter({ dialect: 'sqlite', storage: process.env.DB_PATH }),
	model: {
		name: 'category',
		define: {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
			},
			index: Sequelize.INTEGER,
			label: Sequelize.STRING,
			description: Sequelize.STRING,
		},
		options: {
			timestamps: false
		}
	},
	settings: {

		entityValidator: {
			label: "string|min:3",
		},
	},

	hooks: {
		before: {
			/**
			 * Register a before hook for the `create` action.
			 * It sets a default value for the quantity field.
			 *
			 * @param {Context} ctx
			 */
			create(ctx) {
			}
		}
	},

	/**
	 * Actions
	 */
	actions: {

	},


	methods: {

	},


	async afterConnected() {
		// await this.adapter.collection.createIndex({ name: 1 });
	}
};
