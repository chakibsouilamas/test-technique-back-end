"use strict";

const DbMixin = require("../mixins/db.mixin");
const SqlAdapter = require("moleculer-db-adapter-sequelize");
const Sequelize = require("sequelize");
const dotenv = require('dotenv');

dotenv.config();

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "products",
	mixins: [DbMixin("product")],
	adapter: new SqlAdapter({ dialect: 'sqlite', storage: process.env.DB_PATH }),
	model: {
		name: 'product',
		define: {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
			},
			label: Sequelize.STRING,
			description: Sequelize.STRING,
			price: Sequelize.FLOAT,
			category_id: {
				type: Sequelize.INTEGER,
				references: {
					model: 'categories',
					key: 'id'
				}
			},

			thumbnail_url: Sequelize.STRING,
			visible_public: Sequelize.BOOLEAN,
			visible_authenticated: Sequelize.BOOLEAN
		},
		options: {
			timestamps: false
		}
	},
	settings: {
		populates: {
			'category': {
				field: "category_id", action: "categories.get"
			}
		},
		// Validator for the `create` & `insert` actions.
		entityValidator: {
			name: "string|min:3",
			price: "number|positive"
		},
	},

	hooks: {
		after: {
			/**
			 * Register a before hook for the `create` action.
			 * It sets a default value for the quantity field.
			 *
			 * @param {Context} ctx
			 */
			list(ctx, res) {
				if (!ctx.meta.jwtDecoded) {
					return res.rows.filter(x => x.visible_public);
				} else {
					return res.rows.filter(x => x.visible_authenticated);
				}
			}
		}
	},

	/**
	 * Actions
	 */
	actions: {

	},


	methods: {

	},


	async afterConnected() {
		// await this.adapter.collection.createIndex({ name: 1 });
	}
};
