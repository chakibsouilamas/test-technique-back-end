"use strict";

const DbMixin = require("../mixins/db.mixin");
const SqlAdapter = require("moleculer-db-adapter-sequelize");
const Sequelize = require("sequelize");
const dotenv = require('dotenv');

dotenv.config();

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "carts",
	mixins: [DbMixin("cart")],
	adapter: new SqlAdapter({ dialect: 'sqlite', storage: process.env.DB_PATH }),
	model: {
		name: 'cart',
		define: {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
			},
			user_id: {
				type: Sequelize.INTEGER,
				references: {
					model: 'users',
					key: '_id'
				}
			},
			product:  {
				type: Sequelize.INTEGER,
				references: {
					model: 'products',
					key: 'id'
				}
			},
			total: Sequelize.FLOAT,
		},
		options: {
			timestamps: false
		}
	},
	settings: {
		populates: {
			'user': {
				field: "user_id", action: "users.get"
			}
		},
		// Validator for the `create` & `insert` actions.
		entityValidator: {
			total: "number|positive"
		},
	},

	hooks: {
		before: {
			/**
			 * Register a before hook for the `create` action.
			 * It sets a default value for the quantity field.
			 *
			 * @param {Context} ctx
			 */
			create(ctx) {
			}
		}
	},

	/**
	 * Actions
	 */
	actions: {

	},


	methods: {

	},


	async afterConnected() {
		// await this.adapter.collection.createIndex({ name: 1 });
	}
};
