# back-api-airweb
Projet en microservices sous moleculer-js - pour AirWeb

## Utilisation
- Démarrez le projet avec la commande `npm run dev`. 
- Après le démarrage, ouvrez l'URL http://localhost:8089/ dans votre navigateur. 
- Sur la page d'accueil, vous pouvez tester les services générés via API Gateway et vérifier les nœuds et les services.
- Ne pas oublier un petit coup de `npm i`

## Les end-points
- REST `localhost:8089/api/products`
- REST `localhost:8089/api/categories`
- REST `localhost:8089/api/users`
- POST `localhost:8089/api/users/login?email=jeanmichel@airweb.fr&password=bonjour`
- REST `localhost:8089/api/carts`

## Quelques détails

- L'end-point login génère un token (Bearer ) à ajouter au header.
- Un crud à été développé pour chaque entité
- Un crud pour le panier a été développé
- docker file  & yaml de déploiment K8S joints au projet


## ATTENTION : 
La version du mot de passe hachée coté app n'est pas le même que celle stockée en base.

`Author` : `Chakib SOUILAMAS`
