const Sequelize = require("sequelize");
const User = {
	name: 'user',
	define: {
		_id: Sequelize.STRING,
		name: Sequelize.STRING,
		email: Sequelize.STRING,
		password_hash: Sequelize.STRING
	}
};
module.exports = { User };



