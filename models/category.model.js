const Sequelize = require("sequelize");
const Category = {
	name: 'category',
	define: {
		id: Sequelize.STRING,
		index: Sequelize.INTEGER,
		label: Sequelize.STRING,
		description: Sequelize.STRING,
	}
};
module.exports = { Category };



