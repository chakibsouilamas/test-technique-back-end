const Sequelize = require("sequelize");
const Product = {
	name: 'product',
	define: {
		id: Sequelize.STRING,
		label: Sequelize.STRING,
		description: Sequelize.STRING,
		price: Sequelize.FLOAT,
		category_id: Sequelize.STRING,
		thumbnail_url: Sequelize.STRING,
		visible_public: Sequelize.BOOLEAN,
		visible_authenticated: Sequelize.BOOLEAN
	},
};
module.exports = { Product };



